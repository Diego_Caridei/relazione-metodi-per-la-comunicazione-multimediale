# METODI PER LA COMUNICAZIONE MULTIMEDIALE #
Il corso è finalizzato a far acquisire agli allievi la conoscenza dei fondamenti di teoria dell’informazione ed elaborazione dei segnali per la comunicazione multimediale. L’attenzione è pertanto incentrata sui principali metodi di compressione (con e senza perdita) per dati multimediali (segnale audio, immagini e video).
#Relazione#
La relazione è incentrata sullo studio e la comparazione dei vari metodi compressione audio (MPEG1, MPEG2 e MPEG 4)
![1776164619-1.png](https://bitbucket.org/repo/6XaMn6/images/3510293426-1776164619-1.png)

Relazione a cura di  Diego Caridei e Fabio Nisci.